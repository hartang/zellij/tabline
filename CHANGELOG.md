# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
These are the categories specified by Keep a Changelog. Please pick a suitable
one and don't make up any for yourself.

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
-->

## Unreleased


## 0.2.0 - 2024-06-05

### Changed
- Display sync/fullscreen state with icons rather than text (!4)


## 0.1.1 - 2024-05-03

### Fixed
- Define entry point in `main.rs`, so it's picked up by zellij WASM glue code.


## Initial release - 2024-05-03

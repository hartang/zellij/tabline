---
# Prevent duplicate pipelines for branches, tags and merge requests
workflow:
  rules:
    # Don't run pipeline when commit belongs to a branch AND has a tag
    - if: $CI_COMMIT_TAG && $CI_COMMIT_BRANCH
      when: never
    # Tag pipeline only
    - if: $CI_COMMIT_TAG
    # MR pipelines only
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Don't run pipeline when commit belongs to a branch that has an open MR
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    # Regular branch pipeline (no tag, no MR open)
    - if: $CI_COMMIT_BRANCH

include:
  - project: "hartang/ci-snippets"
    ref: "main"
    file:
      - "rust/common.yml"
      - "release/common.yml"

stages:
  - build
  - test
  - publish
  - release

variables:
  RELEASE_ARTIFACTS_DIR: "${CI_PROJECT_DIR}/artifacts"

.on_tag: &on_tag
  rules:
    - if: $CI_COMMIT_TAG

.rust_wasm: &rust_wasm
  before_script:
    - rustup target add wasm32-wasi


Build (stable):
  stage: build
  extends: .rust-build
  <<: *rust_wasm

Build (nightly):
  stage: build
  variables:
    CARGO_NET_OFFLINE: "false"
  extends: .rust-build-nightly
  <<: *rust_wasm

Code formatting:
  stage: test
  extends: .rust-formatting
  <<: *rust_wasm

Run tests:
  stage: test
  image: "docker.io/library/rust:latest"
  variables:
    CARGO_NET_OFFLINE: "false"
  script:
    - cargo --version
    - cargo test --workspace --target x86_64-unknown-linux-gnu

Clippy lints:
  stage: test
  extends: .rust-clippy
  <<: *rust_wasm

Build Artifacts:
  stage: build
  image: docker.io/library/rust:latest
  variables:
    CARGO_NET_OFFLINE: "false"
  script:
    - rustc --version && cargo --version
    - >
      cargo build --workspace --release --locked --target wasm32-wasi
      --bin tabline
    - >
      install -Dm755
      "target/wasm32-wasi/release/tabline.wasm"
      "${RELEASE_ARTIFACTS_DIR}/tabline.zj"
  artifacts:
    paths:
      - "${RELEASE_ARTIFACTS_DIR}/"
  <<: *on_tag
  <<: *rust_wasm

Publish Artifacts:
  stage: publish
  extends: .release-publish-artifacts
  variables:
    ARTIFACTS_DIR: "${RELEASE_ARTIFACTS_DIR}"
  <<: *on_tag

Create Release:
  stage: release
  extends: .release-create-release
  <<: *on_tag

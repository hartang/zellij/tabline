// Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

use anyhow::Context;
use std::{collections::BTreeMap, str::FromStr};

/// Application configuration.
///
/// At present the config is read only once during plugin startup and is static for the entire
/// sessions lifetime.
#[derive(Debug)]
pub struct Config {
    /// See [`crate::theme::ThemeStyles`].
    pub style_tab: Option<StyleConfig>,
    /// See [`crate::theme::ThemeStyles`].
    pub style_tab_selected: Option<StyleConfig>,
    /// See [`crate::theme::ThemeStyles`].
    pub style_session: Option<StyleConfig>,
    /// See [`crate::theme::ThemeStyles`].
    pub style_mode: Option<StyleConfig>,
    /// See [`crate::theme::ThemeStyles`].
    pub style_swap_layout: Option<StyleConfig>,
    /// See [`crate::theme::ThemeStyles`].
    pub style_error: Option<StyleConfig>,
    /// See [`crate::theme::ThemeColors`].
    pub style_bg_color: Option<TextColor>,
    /// If true, this plugin is not *selectable*.
    ///
    /// **NOTE**: If this is true, you must set the required permissions manually by editing
    /// `~/.cache/zellij/permissions.kdl` yourself!
    pub is_selectable: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            style_tab: None,
            style_tab_selected: None,
            style_session: None,
            style_mode: None,
            style_swap_layout: None,
            style_error: None,
            style_bg_color: None,
            is_selectable: true,
        }
    }
}

impl TryFrom<BTreeMap<String, String>> for Config {
    type Error = anyhow::Error;

    fn try_from(value: BTreeMap<String, String>) -> Result<Self, Self::Error> {
        let mut ret = Config::default();

        for (k, ref v) in value {
            match k.trim() {
                "style_tab" => ret.style_tab = Some(StyleConfig::from_str(v)?),
                "style_tab_selected" => ret.style_tab_selected = Some(StyleConfig::from_str(v)?),
                "style_session" => ret.style_session = Some(StyleConfig::from_str(v)?),
                "style_mode" => ret.style_mode = Some(StyleConfig::from_str(v)?),
                "style_swap_layout" => ret.style_swap_layout = Some(StyleConfig::from_str(v)?),
                "style_error" => ret.style_error = Some(StyleConfig::from_str(v)?),
                "style_bg_color" => ret.style_bg_color = Some(TextColor::from_str(v)?),
                "is_selectable" => ret.is_selectable = v.parse::<bool>()?,
                _ => {
                    anyhow::bail!("found unknown key '{k}' in plugin configuration");
                }
            }
        }

        Ok(ret)
    }
}

/// Configuration equivalent to [`yansi::Style`].
///
/// This type doesn't convey new information, but allows us to implement parsing for
/// foreground/background colors and additional text attributes in a concise format from a single
/// string. This makes configuring through the zellij plugin system easy, since we can only
/// communicate singular strings this way.
///
/// The string input must conform to the following format specification:
///
/// ## General rules
/// - You can set foreground color, background color and text attributes (we call each a *snippet*
///   below)
/// - Each style snippet is separated by a single `+`
/// - The order of snippets is irrelevant
/// - Whitespace and text case is irrelevant
///
/// ## [Text Attributes][`TextAttribute`]
/// - If any text attribute is set, it overrides all attributes from the default theme
/// - Text attributes can be given repeatedly, but do not have an effect beyond their first usage
/// - Text attributes *cannot* be reset or unset
/// - Text attributes *can* be combined freely
/// - Refer to [`TextAttribute`] for valid text attribute inputs.
///
/// ## [Colors][`TextColor`]
/// - Foreground colors are preceded by `fg=`, background colors are preceded by `bg=`
/// - Foreground and background color can be set at most once
/// - Refer to [`TextColor`] for valid color inputs
///
/// # Examples
///
/// ```
/// use tabline::config::StyleConfig;
/// use std::str::FromStr;
/// use yansi::Paint;
///
/// let red_bold = "fg = #ff0000 + bold + underline".parse::<StyleConfig>().unwrap();
/// println!("{}", "Look at me go!".paint(red_bold));
///
/// let green = "fg = #ff0000 + bg = #ffffff".parse::<StyleConfig>().unwrap();
/// println!("{}", "Mean green".paint(green));
///
/// let CAPS = "FG = #ABCDEF + BOLD + UNDERLINE + BLINK".parse::<StyleConfig>().unwrap();
/// println!("{}", "YELL AND SHOUT!!!!".paint(CAPS));
/// ```
#[derive(Debug, Default)]
pub struct StyleConfig {
    pub color_fg: Option<TextColor>,
    pub color_bg: Option<TextColor>,
    pub attributes: Vec<TextAttribute>,
}

impl From<StyleConfig> for yansi::Style {
    fn from(value: StyleConfig) -> Self {
        let mut style = yansi::Style::default();
        if let Some(ref fg) = value.color_fg {
            style = style.fg(fg.into());
        }
        if let Some(ref bg) = value.color_bg {
            style = style.bg(bg.into());
        }
        if !value.attributes.is_empty() {
            let mut new_style = yansi::Style::default();
            if let Some(fg) = style.foreground {
                new_style = new_style.fg(fg);
            }
            if let Some(bg) = style.background {
                new_style = new_style.bg(bg);
            }
            for attr in value.attributes {
                new_style = new_style.attr((&attr).into());
            }
            style = new_style
        }

        style
    }
}

/// Refer to [`StyleConfig`] for an explanation of how string parsing works.
impl std::str::FromStr for StyleConfig {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut ret = Self::default();
        if s.is_empty() {
            return Ok(ret);
        }

        for fragment in s.split('+') {
            let fragment = fragment.trim().to_ascii_lowercase();

            if fragment.starts_with("fg") {
                if ret.color_fg.is_some() {
                    anyhow::bail!("cannot set foreground color twice");
                }
                let Some((keyword, color)) = fragment.split_once('=') else {
                    anyhow::bail!("foreground color specification must be of type 'fg = ...'");
                };
                if keyword.trim() != "fg" {
                    anyhow::bail!("unknown keyword '{keyword}' found while parsing style config");
                }
                let fg_color = TextColor::from_str(color).with_context(|| {
                    format!("failed to parse foreground color from input '{fragment}'")
                })?;

                ret.color_fg = Some(fg_color);
            } else if fragment.starts_with("bg") {
                if ret.color_bg.is_some() {
                    anyhow::bail!("cannot set background color twice");
                }
                let Some((keyword, color)) = fragment.split_once('=') else {
                    anyhow::bail!("background color specification must be of type 'bg = ...'");
                };
                if keyword.trim() != "bg" {
                    anyhow::bail!("unknown keyword '{keyword}' found while parsing style config");
                }
                let bg_color = TextColor::from_str(color).with_context(|| {
                    format!("failed to parse background color from input '{fragment}'")
                })?;

                ret.color_bg = Some(bg_color);
            } else {
                let attr = TextAttribute::from_str(&fragment[..]).with_context(|| {
                    format!("don't know how to handle invalid input '{fragment}'")
                })?;
                ret.attributes.push(attr);
            }
        }

        Ok(ret)
    }
}

/// Configuration equivalent to [`yansi::Color`].
///
/// This wrapper allows us to define our own custom parsing behavior for color codes. It recognizes
/// colors in the following string formats:
///
/// - 6 hex digits with a preceding `#` for true-color codes
/// - An integer number up to 255 (inclusive) for fixed ANSI color codes
///
/// # Examples
///
/// ```
/// use std::str::FromStr;
/// use tabline::config::TextColor;
/// use yansi::Color;
///
/// let red = "#ff0000".parse::<TextColor>().unwrap();
/// assert_eq!(Color::from(&red), yansi::Color::Rgb(255, 0, 0));
///
/// let orange = "214".parse::<TextColor>().unwrap();
/// assert_eq!(Color::from(&orange), yansi::Color::Fixed(214));
///
/// let invalid = "#f00".parse::<TextColor>();
/// assert!(invalid.is_err());
///
/// let invalid = "256".parse::<TextColor>();
/// assert!(invalid.is_err());
/// ```
#[derive(Debug, Default)]
pub enum TextColor {
    #[default]
    None,
    Rgb(u8, u8, u8),
    Fixed(u8),
}

impl From<&TextColor> for yansi::Color {
    fn from(value: &TextColor) -> Self {
        match value {
            TextColor::None => Self::default(),
            TextColor::Rgb(r, g, b) => Self::Rgb(*r, *g, *b),
            TextColor::Fixed(val) => Self::Fixed(*val),
        }
    }
}

/// Refer to [`TextColor`] for an explanation of how string parsing works.
impl std::str::FromStr for TextColor {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim();
        if s.is_empty() {
            Ok(Self::None)
        } else if let Some(s) = s.strip_prefix('#') {
            // Skip the first char
            if s.chars().count() != 6 {
                anyhow::bail!("RGB color codes must be 6 chars long");
            }
            if !s.chars().all(|c| c.is_ascii_hexdigit()) {
                anyhow::bail!("RGB color codes must consist of only hex digits");
            }
            // This should never panic since we check the conditions above.
            let colorcode = u32::from_str_radix(s, 16).unwrap();
            let [_, r, g, b] = colorcode.to_be_bytes();

            Ok(Self::Rgb(r, g, b))
        } else if s.chars().all(|c| c.is_ascii_digit()) {
            if s.chars().count() <= 3 {
                let color = s
                    .parse::<u8>()
                    .context("color number must be less than or equal to 255")?;
                Ok(Self::Fixed(color))
            } else {
                anyhow::bail!("fixed color codes cannot have more than 3 digits");
            }
        } else {
            anyhow::bail!("don't know how to parse '{s}' as valid colorcode");
        }
    }
}

/// Configuration equivalent to [`yansi::Attribute`].
///
/// This wrapper allows us to define our own custom parsing behavior for text attributes. See the
/// examples below for what's supported.
///
/// # Examples
///
/// ```
/// use std::str::FromStr;
/// use tabline::config::TextAttribute;
/// use yansi::Attribute;
///
/// let bold: Attribute = "bold".parse::<TextAttribute>().unwrap().into();
/// assert_eq!(bold, Attribute::Bold);
///
/// let dim: Attribute = "dim".parse::<TextAttribute>().unwrap().into();
/// assert_eq!(dim, Attribute::Dim);
///
/// let italic: Attribute = "italic".parse::<TextAttribute>().unwrap().into();
/// assert_eq!(italic, Attribute::Italic);
///
/// let underline: Attribute = "underline".parse::<TextAttribute>().unwrap().into();
/// assert_eq!(underline, Attribute::Underline);
///
/// let blink: Attribute = "blink".parse::<TextAttribute>().unwrap().into();
/// assert_eq!(blink, Attribute::Blink);
///
/// let invalid = "anything else".parse::<TextAttribute>();
/// assert!(invalid.is_err());
/// ```
#[derive(Debug, strum::Display, strum::EnumIter)]
pub enum TextAttribute {
    #[strum(serialize = "bold")]
    Bold,
    #[strum(serialize = "dim")]
    Dim,
    #[strum(serialize = "italic")]
    Italic,
    #[strum(serialize = "underline")]
    Underline,
    #[strum(serialize = "blink")]
    Blink,
}

impl From<TextAttribute> for yansi::Attribute {
    fn from(value: TextAttribute) -> Self {
        Self::from(&value)
    }
}

impl From<&TextAttribute> for yansi::Attribute {
    fn from(value: &TextAttribute) -> Self {
        match value {
            TextAttribute::Bold => Self::Bold,
            TextAttribute::Dim => Self::Dim,
            TextAttribute::Italic => Self::Italic,
            TextAttribute::Underline => Self::Underline,
            TextAttribute::Blink => Self::Blink,
        }
    }
}

/// Refer to [`TextAttribute`] for an explanation of how string parsing works.
impl std::str::FromStr for TextAttribute {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        for variant in <Self as strum::IntoEnumIterator>::iter() {
            if variant.to_string() == s.trim().to_ascii_lowercase()[..] {
                return Ok(variant);
            }
        }
        anyhow::bail!("failed to parse valid text attribute from '{s}'");
    }
}

// Trick `rustdoc` into handling the crate because `rustdoc` by default treats bin-crates specially
// by entirely ignoring them in absolutely any regard. It refuses to build docs (or even *believe*
// that there are indeed docs to build) and also will not run doctests for bin crates.
//
// It will, however, happily treat anything that's caled `lib.rs` (even in a non-lib crate) and
// anything below that. So we add an almost empty `main.rs` just for the look of the thing (so
// `cargo` still believes that this is indeed a bin crate and doesn't compile a lib) and move the
// content of the former `main.rs` to `lib.rs` for the sake of documentation. Amazing.
//
// If you're looking for the *real* application code, check out `lib.rs`.
use tabline::State;
use zellij_tile::prelude::*;

register_plugin!(State);

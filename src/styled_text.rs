// Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

use yansi::{Paint, Style};

/// A piece of text with backing styling information.
///
/// The styling is stored separately from the text to allow for modification after creation, and to
/// offer improved composition. Next to other things, this allows us to manipulate the raw string
/// without having to think about embedded ANSI escape sequences, and it makes determining the
/// strings length trivial.
#[derive(Debug, Clone, Default)]
pub struct StyledText {
    /// The style to apply to the `text` when printing
    style: Style,
    /// The text to display
    text: String,
    /// The length of the printed `text` in columns
    length: usize,
}

impl StyledText {
    /// Create a new styled text from styling information and a string.
    pub fn new<S: Into<String>>(style: Style, text: S) -> Self {
        let text = text.into();
        Self {
            style,
            length: text.chars().count(),
            text,
        }
    }

    /// Returns true if the contained string is empty.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// The length of the printed string in columns.
    #[inline]
    pub fn len(&self) -> usize {
        self.length
    }

    /// Create a copy of the string with the new style applied.
    pub fn set_style(&self, new_style: Style) -> Self {
        let mut ret = self.clone();
        ret.style = new_style;
        ret
    }

    /// Truncate the given amount of columns from the right of the string.
    ///
    /// Leaves the beginning of the string intact.
    pub fn trunc_columns_right(&self, truncate: usize) -> Self {
        let cols_to_show = self.len().saturating_sub(truncate);
        let s = self.text.chars().take(cols_to_show).collect::<String>();

        debug_assert_eq!(cols_to_show, s.chars().count());

        Self {
            style: self.style,
            text: s,
            length: cols_to_show,
        }
    }

    /// Truncate the given amount of columns from the left of the string.
    ///
    /// Leaves the end of the string intact.
    pub fn trunc_columns_left(&self, truncate: usize) -> Self {
        let cols_to_show = self.len().saturating_sub(truncate);
        let s = self.text.chars().skip(truncate).collect::<String>();

        debug_assert_eq!(cols_to_show, s.chars().count());

        Self {
            style: self.style,
            text: s,
            length: cols_to_show,
        }
    }
}

impl std::fmt::Display for StyledText {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.text.paint(self.style))
    }
}

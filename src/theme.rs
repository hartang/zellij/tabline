// Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

use yansi::{Color, Style};
use zellij_tile::prelude::{Palette, PaletteColor, ThemeHue};

/// Global plugin theme.
#[derive(Debug, Default)]
pub struct Theme {
    pub color: ThemeColors,
    pub style: ThemeStyles,
}

macro_rules! apply_style {
    ($target:expr, $source:expr) => {
        if let Some(ref style_tab) = $source {
            if let Some(ref fg_col) = style_tab.color_fg {
                $target = $target.fg(fg_col.into());
            }
            if let Some(ref bg_col) = style_tab.color_bg {
                $target = $target.bg(bg_col.into());
            }
            if !style_tab.attributes.is_empty() {
                // It appears there is no way to reset attributes on a text, so we have to rebuild
                // the whole thing and copy the colors over...
                let mut new_style = yansi::Style::default();
                if let Some(fg) = $target.foreground {
                    new_style = new_style.fg(fg);
                }
                if let Some(bg) = $target.background {
                    new_style = new_style.bg(bg);
                }
                for attr in &style_tab.attributes {
                    new_style = new_style.attr(attr.into());
                }
                $target = new_style
            }
        }
    };
}

impl Theme {
    /// Merge the current theme with theme values taken from the [plugin
    /// config](crate::config::Config).
    pub fn merge_with_config(mut self, config: &crate::config::Config) -> Self {
        apply_style!(self.style.tab, config.style_tab);
        apply_style!(self.style.tab_selected, config.style_tab_selected);
        apply_style!(self.style.swap_layout, config.style_swap_layout);
        apply_style!(self.style.error, config.style_error);
        apply_style!(self.style.mode, config.style_mode);
        apply_style!(self.style.session, config.style_session);
        if let Some(ref col_bg) = config.style_bg_color {
            self.color.bg = col_bg.into();
        }
        self
    }
}

/// Color definitions for various UI elements
#[derive(Debug)]
pub struct ThemeColors {
    /// Background color of empty space in the tabline.
    pub bg: Color,
}

impl Default for ThemeColors {
    fn default() -> Self {
        Self { bg: Color::Black }
    }
}

/// Style definitions for Text elements in the UI
#[derive(Debug)]
pub struct ThemeStyles {
    /// Style for the tab ribbon texts (e.g. tab names)
    pub tab: Style,
    /// Style for the tab ribbon texts on active tab
    pub tab_selected: Style,
    /// Style for the session name in session mode
    pub session: Style,
    /// Style for the mode when not in normal mode
    pub mode: Style,
    /// Style for the swap layout name in pane mode
    pub swap_layout: Style,
    /// Style for error messages
    pub error: Style,
}

impl Default for ThemeStyles {
    fn default() -> Self {
        Self {
            tab: Style::default().bg(Color::Black).fg(Color::White).italic(),
            tab_selected: Style::default().bg(Color::Yellow).fg(Color::Black).bold(),
            session: Style::default().bg(Color::Black).fg(Color::Red).italic(),
            mode: Style::default().bg(Color::Black).fg(Color::Green),
            swap_layout: Style::default().bg(Color::Black).fg(Color::Blue),
            error: Style::default().bg(Color::Black).fg(Color::Red).bold(),
        }
    }
}

/// Convert a [zellij color](`PaletteColor`) to a [yansi color](`Color`).
fn to_yansi(col: &PaletteColor) -> Color {
    match col {
        PaletteColor::EightBit(val) => Color::Fixed(*val),
        PaletteColor::Rgb((r, g, b)) => Color::Rgb(*r, *g, *b),
    }
}

/// Construct the default plugin theme from the [zellij theme](Palette).
impl From<&Palette> for Theme {
    fn from(value: &Palette) -> Self {
        let (hue_col, inv_hue_col) = match value.theme_hue {
            ThemeHue::Dark => (to_yansi(&value.black), to_yansi(&value.white)),
            ThemeHue::Light => (to_yansi(&value.white), to_yansi(&value.black)),
        };
        Self {
            color: ThemeColors { bg: hue_col },
            style: ThemeStyles {
                tab: Style::default().bg(hue_col).fg(inv_hue_col).italic(),
                tab_selected: Style::default()
                    .bg(to_yansi(&value.green))
                    .fg(hue_col)
                    .bold(),
                session: Style::default()
                    .bg(hue_col)
                    .fg(to_yansi(&value.magenta))
                    .italic(),
                mode: Style::default()
                    .bg(hue_col)
                    .fg(to_yansi(&value.orange))
                    .bold(),
                swap_layout: Style::default().bg(hue_col).fg(to_yansi(&value.red)).bold(),
                error: Style::default().bg(hue_col).fg(to_yansi(&value.red)).bold(),
            },
        }
    }
}

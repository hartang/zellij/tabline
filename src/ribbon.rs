// Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::styled_text::StyledText;

/// A ribbon or tab line segment.
///
/// These are automatically created from a "content" string by stripping all ANSI sequences and
/// calculating the length of the remaining characters. This is important because we assemble
/// fully-themed strings, but need to know the size to handle e.g. tabline overflow.
#[derive(Debug, Default, Clone)]
pub struct Ribbon {
    /// The actual content of the ribbon
    content: Vec<StyledText>,
    /// Total printed length of the ribbon (in columns)
    len: usize,
}

impl Ribbon {
    /// Create a new ribbon from a [styled text](StyledText).
    pub fn new<S: Into<StyledText>>(s: S) -> Self {
        let content = s.into();
        let len = content.len();
        Self {
            content: vec![content],
            len,
        }
    }

    /// Get the printed length of the ribbon (in columns).
    ///
    /// # Examples
    ///
    /// ```
    /// use tabline::{ribbon::Ribbon, styled_text::StyledText};
    /// use yansi::Style;
    ///
    /// let s = StyledText::new(Style::default().red(), "red");
    /// let mut r = Ribbon::new(s);
    /// assert_eq!(r.len(), 3);
    ///
    /// r.append(StyledText::new(Style::default().green(), "blue?"));
    /// assert_eq!(r.len(), 8);
    /// ```
    pub fn len(&self) -> usize {
        self.len
    }

    /// True if the ribbon is empty
    ///
    /// # Examples
    ///
    /// ```
    /// use tabline::{ribbon::Ribbon, styled_text::StyledText};
    /// use yansi::Style;
    ///
    /// let style = Style::default().blue();
    /// let mut r = Ribbon::new(StyledText::new(style, ""));
    /// assert!(r.is_empty());
    ///
    /// r.append(StyledText::new(Style::default(), "bla"));
    /// assert!(!r.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Prepend a styled text to the ribbon.
    pub fn prepend<S: Into<StyledText>>(&mut self, s: S) {
        let fragment = s.into();
        self.len += fragment.len();
        self.content.insert(0, fragment);
    }

    /// Append a styled text to the ribbon.
    pub fn append<S: Into<StyledText>>(&mut self, s: S) {
        let fragment = s.into();
        self.len += fragment.len();
        self.content.push(fragment);
    }

    /// Truncate the given amount of columns from the right of the string.
    ///
    /// Leaves the beginning of the string intact.
    ///
    /// # Examples
    ///
    /// ```
    /// use tabline::{ribbon::Ribbon, styled_text::StyledText};
    /// use yansi::Style;
    ///
    /// let style = Style::default();
    /// let ribbon = Ribbon::new(StyledText::new(style, "987654321"));
    /// assert_eq!(ribbon.trunc_columns_right(0).to_string(), "987654321");
    /// assert_eq!(ribbon.trunc_columns_right(3).to_string(), "987654");
    /// assert_eq!(ribbon.trunc_columns_right(6).to_string(), "987");
    /// assert_eq!(ribbon.trunc_columns_right(9).to_string(), "");
    /// assert_eq!(ribbon.trunc_columns_right(100).to_string(), "");
    /// ```
    pub fn trunc_columns_right(&self, truncate: usize) -> Self {
        let mut fragments = vec![];
        let mut chars_left: usize = self.len().saturating_sub(truncate);

        for f in &self.content {
            let to_append = if f.len() <= chars_left {
                f.clone()
            } else {
                f.trunc_columns_right(f.len().saturating_sub(chars_left))
            };
            chars_left -= to_append.len();
            fragments.push(to_append);
        }

        let new_len = fragments.iter().map(|f| f.len()).sum();
        debug_assert_eq!(new_len, self.len().saturating_sub(truncate));

        Self {
            len: new_len,
            content: fragments,
        }
    }

    /// Truncate the given amount of columns from the left of the string.
    ///
    /// Leaves the end of the string intact.
    ///
    /// # Examples
    ///
    /// ```
    /// use tabline::{ribbon::Ribbon, styled_text::StyledText};
    /// use yansi::Style;
    ///
    /// let style = Style::default();
    /// let ribbon = Ribbon::new(StyledText::new(style, "123456789"));
    /// assert_eq!(ribbon.trunc_columns_left(0).to_string(), "123456789");
    /// assert_eq!(ribbon.trunc_columns_left(3).to_string(), "456789");
    /// assert_eq!(ribbon.trunc_columns_left(6).to_string(), "789");
    /// assert_eq!(ribbon.trunc_columns_left(9).to_string(), "");
    /// assert_eq!(ribbon.trunc_columns_left(100).to_string(), "");
    /// ```
    pub fn trunc_columns_left(&self, truncate: usize) -> Self {
        let mut fragments = vec![];
        let mut chars_left: usize = self.len().saturating_sub(truncate);

        for f in self.content.iter().rev() {
            let to_append = if f.len() <= chars_left {
                f.clone()
            } else {
                f.trunc_columns_left(f.len().saturating_sub(chars_left))
            };
            chars_left -= to_append.len();
            fragments.insert(0, to_append);
        }

        let new_len = fragments.iter().map(|f| f.len()).sum();
        debug_assert_eq!(new_len, self.len().saturating_sub(truncate));

        Self {
            len: new_len,
            content: fragments,
        }
    }
}

/// Apply the stored styling information and display the styled string.
impl std::fmt::Display for Ribbon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for c in &self.content {
            write!(f, "{}", c)?;
        }
        Ok(())
    }
}

impl From<StyledText> for Ribbon {
    fn from(value: StyledText) -> Self {
        Self::new(value)
    }
}

impl From<Vec<StyledText>> for Ribbon {
    fn from(value: Vec<StyledText>) -> Self {
        let total_len = value.iter().map(|x| x.len()).sum();
        Self {
            content: value,
            len: total_len,
        }
    }
}

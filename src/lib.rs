// Copyright (C) 2024 Andreas Hartmann <hartan@7x.de>
// GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
// SPDX-License-Identifier: GPL-3.0-or-later

use std::io::Write;
use zellij_tile::prelude::*;

pub mod config;
pub mod ribbon;
pub mod styled_text;
pub mod theme;

use config::Config;
use ribbon::Ribbon;
use std::collections::BTreeMap;
use styled_text::StyledText;
use theme::Theme;
use yansi::Style;

const SEPERATOR: &str = "";
const TRAILER_SEPERATOR: &str = "";
const ELLIPSIS: &str = "...";

/// Get a styled start separator (start of tab ribbon).
pub fn sep_start(theme: &Theme) -> StyledText {
    StyledText::new(theme.style.tab_selected.invert(), SEPERATOR)
}

/// Get a styled end separator (end of tab ribbon).
pub fn sep_end(theme: &Theme) -> StyledText {
    StyledText::new(theme.style.tab_selected, SEPERATOR)
}

/// Get uniform, styled padding string of specified length.
pub fn pad(theme: &Theme, len: usize) -> StyledText {
    let pad_str = format!("{:1$}", " ", len);
    StyledText::new(Style::default().bg(theme.color.bg), pad_str)
}

/// Global state of the plugin.
#[derive(Default)]
pub struct State {
    /// The list of currently existing tabs
    tabs: Tabs,
    /// Plugin configuration
    config: Config,
    /// Captured one-time event, if any
    ///
    /// This is automatically cleared after having been rendered.
    event: Option<OneTimeEvent>,
    /// Information and state of the current zellij mode
    mode_info: ModeInfo,
    /// Theme to apply to UI elements
    theme: Theme,
    /// Scroll for calculating the current view of the tabline when tabs don't fir the screen
    view_scroll: usize,
}

/// One-time events, to be displayed instead of tabs if any.
#[derive(Debug)]
enum OneTimeEvent {
    CopyToClipboard(CopyDestination),
    FailedToCopy,
}

/// Container for the currently existing tabs.
///
/// This allows us to implement our own custom methods on it.
#[derive(Default)]
struct Tabs {
    inner: Vec<TabInfo>,
}

impl Tabs {
    /// Create a vector of themed ribbons from all existing tabs.
    ///
    /// Each tab is turned into a separate [`Ribbon`].
    pub fn to_ribbon(&self, theme: &theme::Theme) -> Vec<Ribbon> {
        let mut ret = vec![];
        for t in &self.inner {
            ret.push(to_ribbon(t, theme));
        }
        ret
    }

    /// Get a ref to the currently active tab, if known.
    pub fn get_active_tab(&self) -> Option<&TabInfo> {
        self.inner.iter().find(|t| t.active)
    }
}

impl From<Vec<TabInfo>> for Tabs {
    fn from(value: Vec<TabInfo>) -> Self {
        Self { inner: value }
    }
}

/// Create a ribbon for a single tab.
///
/// The ribbon includes start and end separator, the tabs name and the *sync* and *fullscreen*
/// state, if set.
fn to_ribbon(tab: &TabInfo, theme: &theme::Theme) -> Ribbon {
    let more = match (tab.is_fullscreen_active, tab.is_sync_panes_active) {
        (true, true) => "  ",
        (true, false) => " ",
        (false, true) => " ",
        (false, false) => "",
    };

    // TODO: Implement client cursors
    let mut out = Ribbon::default();

    if tab.active {
        out.append(StyledText::new(
            theme.style.tab_selected,
            format!("  {more}{name}  ", name = tab.name, more = more),
        ));
    } else {
        out.append(sep_start(theme));
        out.append(StyledText::new(
            theme.style.tab,
            format!(" {more}{name} ", name = tab.name, more = more),
        ));
        out.append(sep_end(theme));
    }

    out
}

impl ZellijPlugin for State {
    fn load(&mut self, configuration: BTreeMap<String, String>) {
        self.config = match Config::try_from(configuration) {
            Ok(c) => c,
            Err(e) => {
                eprintln!("An error in the config was detected:");
                eprintln!("{:?}", e);
                eprintln!("Falling back to default config...");
                Config::default()
            }
        };
        set_selectable(self.config.is_selectable);
        request_permission(&[PermissionType::ReadApplicationState]);
        subscribe(&[
            EventType::ModeUpdate,
            EventType::TabUpdate,
            EventType::CopyToClipboard,
        ]);
    }

    fn update(&mut self, event: Event) -> bool {
        let mut should_render = false;
        match event {
            Event::ModeUpdate(mode_info) => {
                // TODO(hartan): Maybe hash the `mode_info` here? And only override the stuff below
                // when that hash changes? Or create a `BTreeMap<Hash, (ModeInfo, Theme)>`?
                let theme = Theme::from(&mode_info.style.colors);
                let theme = theme.merge_with_config(&self.config);
                self.theme = theme;
                self.mode_info = mode_info;
                should_render = true;
            }
            Event::TabUpdate(tabs_info) => {
                self.tabs = Tabs::from(tabs_info);
                should_render = true;
            }
            Event::CopyToClipboard(dest) => {
                self.event = Some(OneTimeEvent::CopyToClipboard(dest));
                should_render = true;
            }
            Event::SystemClipboardFailure => {
                self.event = Some(OneTimeEvent::FailedToCopy);
                should_render = true;
            }
            _ => (),
        };
        should_render
    }

    fn render(&mut self, _rows: usize, cols: usize) {
        let mode_ribbon = self.mode_ribbon();

        // diverging modes: These don't create the "regular" tab bar.
        match self.mode_info.mode {
            InputMode::Locked => {
                let msgwidth = mode_ribbon.len();
                let termwidth = cols;
                let pad_total = termwidth.saturating_sub(msgwidth);

                let pad_right = pad_total / 2;
                let mut arrow_right = Ribbon::new(sep_end(&self.theme));
                arrow_right.append(
                    pad(&self.theme, pad_right - 1).set_style(self.theme.style.tab_selected),
                );

                let pad_left = pad_total - pad_right;
                let mut arrow_left = Ribbon::new(
                    pad(&self.theme, pad_left - 1).set_style(self.theme.style.tab_selected),
                );
                arrow_left.append(sep_start(&self.theme));

                print!("{}{}{}", arrow_left, mode_ribbon, arrow_right);
                return;
            }
            InputMode::EnterSearch => {
                let pad_len = cols.saturating_sub(mode_ribbon.len());
                let pad = pad(&self.theme, pad_len);
                let trunc_len = mode_ribbon.len().saturating_sub(cols);
                print!("{}{pad}", mode_ribbon.trunc_columns_right(trunc_len));
                return;
            }
            _ => (),
        }

        let trailer_ribbon = self.trailer(mode_ribbon);
        let trailer_len = trailer_ribbon.len();

        let content_ribbons = if let Some(ref event) = self.event.take() {
            let mut output = Ribbon::new(sep_start(&self.theme));
            match event {
                OneTimeEvent::CopyToClipboard(dest) => {
                    let dest = match dest {
                        CopyDestination::Command => "external command",
                        CopyDestination::System => "system clipboard",
                        CopyDestination::Primary => "primary selection",
                    };
                    output.append(StyledText::new(
                        self.theme.style.tab,
                        format!(" content copied to {} ", dest),
                    ));
                }
                OneTimeEvent::FailedToCopy => {
                    output.append(StyledText::new(
                        self.theme.style.error,
                        "failed to copy content".to_string(),
                    ));
                }
            }
            output.append(sep_end(&self.theme));
            vec![output]
        } else {
            self.tabs.to_ribbon(&self.theme)
        };
        let content_len = content_ribbons.iter().map(|r| r.len()).sum::<usize>();

        let mut stdout = std::io::stdout().lock();
        // The maximum allowed length of the content
        let content_max_len = (cols as i64) - (trailer_len as i64);
        let trailer_ellipsis = StyledText::new(self.theme.style.tab, format!("{} ", ELLIPSIS));

        if content_max_len <= (trailer_ellipsis.len() as i64) {
            // We must truncate the trailer!
            let truncate_by = (trailer_ellipsis.len() as i64).saturating_sub(content_max_len);
            write!(
                stdout,
                "{}{}",
                trailer_ellipsis,
                trailer_ribbon.trunc_columns_left(truncate_by as usize)
            )
            .unwrap();
        } else if content_len <= (content_max_len.unsigned_abs() as usize) {
            let arrow_tip = sep_start(&self.theme);

            let pad_len = (content_max_len.unsigned_abs() as usize)
                .saturating_sub(content_len)
                .saturating_sub(arrow_tip.len());
            let pad = pad(&self.theme, pad_len);
            for c in &content_ribbons {
                write!(stdout, "{c}").unwrap()
            }
            write!(stdout, "{arrow_tip}{pad}{trailer_ribbon}",).unwrap();
        } else {
            let content_max_len = content_max_len as usize;
            // How far to scroll the view
            let mut new_scroll: Option<usize> = None;
            // How far we may scroll at most
            let new_scroll_max = content_len - content_max_len;
            // Clamp tabline scroll to valid range
            self.view_scroll = std::cmp::min(self.view_scroll, new_scroll_max);

            // FIXME(hartan): Determine where the "current" tab is located. Since this may also be
            // a one-time message, we have to check whether the current tabs index is indeed valid.
            // This feels like a hack and it probably breaks if, I dunno, the one-time message
            // suddenly has more than one ribbon... But it works for now and I hate coding
            // "scrolling" into tab-bars.
            let focused_tab = std::cmp::min(
                content_ribbons.len().saturating_sub(1),
                self.tabs.get_active_tab().map(|t| t.position).unwrap_or(0),
            );
            // Extra text (e.g. ellipsis) to show at start and end of tabline
            let mut line_start = StyledText::new(self.theme.style.tab, format!("{} ", ELLIPSIS));
            let mut line_end = StyledText::new(self.theme.style.tab, format!(" {} ", ELLIPSIS));

            let tab_start_col = |idx: usize| -> usize {
                content_ribbons
                    .iter()
                    .take(idx)
                    .map(|t| t.len())
                    .sum::<usize>()
            };
            let tab_end_col = |idx: usize| -> usize {
                let Some(the_tab) = content_ribbons.get(idx) else {
                    return 0;
                };
                tab_start_col(idx) + the_tab.len()
            };
            let tab_in_view = |idx: usize, start_col: usize, end_col: usize| {
                let tab_start_col = tab_start_col(idx);
                let tab_end_col = tab_end_col(idx);

                (tab_start_col >= start_col) && (tab_end_col <= end_col)
            };

            // Sorry clippy, but this makes the code much more readable IMO.
            #[allow(clippy::collapsible_else_if)]
            if focused_tab == 0 {
                // We're seeing the first tab
                line_start = StyledText::default();
                new_scroll = Some(0);
            } else if focused_tab == content_ribbons.len().saturating_sub(1) {
                // We're seeing the last tab
                line_end = sep_start(&self.theme);
                new_scroll = Some(new_scroll_max);
            } else {
                if self.view_scroll == 0 {
                    if tab_in_view(focused_tab, 0, content_max_len - line_end.len()) {
                        // No scrolling needed
                        line_start = StyledText::default();
                    } else {
                        // Must scroll to the right
                        new_scroll =
                            Some(tab_end_col(focused_tab) - (content_max_len - line_end.len()));
                    }
                } else if self.view_scroll == new_scroll_max {
                    if tab_in_view(
                        focused_tab,
                        new_scroll_max + line_start.len(),
                        new_scroll_max + content_max_len,
                    ) {
                        // No scrolling needed
                        line_end = StyledText::default();
                    } else {
                        // Must scroll to the left
                        new_scroll = Some(tab_start_col(focused_tab) - line_start.len());
                    }
                } else {
                    let view_start = self.view_scroll + line_start.len();
                    let view_end = self.view_scroll + content_max_len - line_end.len();

                    if tab_in_view(focused_tab, view_start, view_end) {
                        // No scrolling needed
                    } else if tab_start_col(focused_tab) < view_start {
                        // Scroll to the left
                        new_scroll = Some(tab_start_col(focused_tab) - line_start.len());
                    } else {
                        // Scroll to the right
                        new_scroll =
                            Some(tab_end_col(focused_tab) - (content_max_len - line_end.len()));
                    }
                }
            }

            if let Some(scroll) = new_scroll {
                self.view_scroll = scroll;
            }

            let view_start = self.view_scroll + line_start.len();
            let view_end = self.view_scroll + content_max_len - line_end.len();

            write!(stdout, "{line_start}").unwrap();
            for (idx, tab) in content_ribbons.iter().enumerate() {
                if tab_in_view(idx, view_start, view_end) {
                    // Tab is completely in view
                    write!(stdout, "{tab}").unwrap();
                } else if (tab_end_col(idx) < view_start) || (tab_start_col(idx) > view_end) {
                    // Not visible
                } else if tab_start_col(idx) < view_start {
                    // Clip from the left
                    write!(
                        stdout,
                        "{}",
                        tab.trunc_columns_left(view_start - tab_start_col(idx))
                    )
                    .unwrap();
                } else if tab_end_col(idx) > view_end {
                    // Clip from the right
                    write!(
                        stdout,
                        "{}",
                        tab.trunc_columns_right(tab_end_col(idx) - view_end)
                    )
                    .unwrap();
                }
            }
            write!(stdout, "{line_end}{trailer_ribbon}").unwrap();
        }
    }
}

impl State {
    /// Create a ribbon for the currently active zellij mode.
    fn mode_ribbon(&self) -> StyledText {
        let msg = match self.mode_info.mode {
            InputMode::Search => "SEARCH",
            InputMode::RenamePane => "RENAME PANE",
            InputMode::RenameTab => "RENAME TAB",
            InputMode::Session => "SESSION",
            InputMode::Move => "MOVE",
            InputMode::Scroll => "SCROLL",
            InputMode::Resize => "RESIZE",
            InputMode::EnterSearch => "ENTERING SEARCH TERM",
            InputMode::Locked => "LOCKED",
            InputMode::Pane => "PANE",
            InputMode::Tab => "TAB",
            _ => return StyledText::default(),
        };

        StyledText::new(self.theme.style.mode, format!(" {} ", msg))
    }

    /// Create the ribbon for the tabline trailer.
    ///
    /// The trailer shows additional information beyond the tab ribbons, including:
    ///
    /// - the currently active zellij mode (if **not** *NORMAL*)
    /// - the swap layout (in *PANE* mode)
    /// - the session name (in *SESSION* mode)
    fn trailer(&self, mode_ribbon: StyledText) -> Ribbon {
        let mut trailer_ribbon = match self.mode_info.mode {
            InputMode::Session => Ribbon::from(vec![
                mode_ribbon,
                StyledText::new(
                    self.theme.style.session,
                    format!(
                        "({}) ",
                        self.mode_info
                            .session_name
                            .as_ref()
                            .unwrap_or(&"N/A".to_string())
                    ),
                ),
            ]),
            InputMode::Pane => {
                let Some(active_tab) = self.tabs.get_active_tab() else {
                    return StyledText::new(self.theme.style.error, "no active tab").into();
                };
                let mut content = Ribbon::new(mode_ribbon);
                if let Some(ref name) = active_tab.active_swap_layout_name {
                    content.append(StyledText::new(
                        self.theme.style.swap_layout,
                        format!("({name}) "),
                    ));
                }
                content
            }
            _ => {
                if mode_ribbon.is_empty() {
                    Ribbon::default()
                } else {
                    mode_ribbon.into()
                }
            }
        };
        if !trailer_ribbon.is_empty() {
            trailer_ribbon.prepend(StyledText::new(
                self.theme.style.tab_selected,
                TRAILER_SEPERATOR,
            ));
            trailer_ribbon.prepend(StyledText::new(
                self.theme.style.tab_selected.invert(),
                TRAILER_SEPERATOR,
            ));
            trailer_ribbon.append(StyledText::new(
                self.theme.style.tab_selected.invert(),
                TRAILER_SEPERATOR,
            ));
        }

        trailer_ribbon
    }
}

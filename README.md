# Tabline - A tabline plugin for zellij

The goal of tabline is to provide a simple and concise overview of open tabs in
a zellij session.

I'm developing this plugin primarily to suit my own needs, so many aspects of
it are likely opinionated. Some of my design decisions also won't make a lot of
sense [without my zellij config][zellij_config].

<!--toc:start-->
- [Tabline - A tabline plugin for zellij](#tabline-a-tabline-plugin-for-zellij)
  - [Installation](#installation)
  - [Comparison to default zellij](#comparison-to-default-zellij)
  - [Development](#development)
    - [Building](#building)
    - [Testing](#testing)
  - [But why?](#but-why)
<!--toc:end-->


## Installation

1. Add the following snippet to your [`config.kdl`][zellij_docs_plugin_alias]
   file (replace `$TAG` with any released tag you wish):
   ```kdl
   plugins {
       // ...
       tabline location="https://gitlab.com/api/v4/projects/53442724/packages/generic/release/$TAG/tabline.zj"
   }
   ```

> NOTE: If there is a `plugin` section in your config file already, just add
> the `tabline ...` line in there.

2. Include the plugin in a [layout file][zellij_docs_layout]:
   ```kdl
   layout {
       pane size=1 borderless=true {
           plugin location="tabline"
       }
       pane
   }
   ```

3. Launch a new zellij session with the layout from above and grant the
   permissions the plugin asks you for
4. Enjoy!


## Comparison to default zellij

Compared to the default zellij layout with tab-bar and status-bar, it offers
the following:

- **No key-binding hints**: The UI hints and tips are great, but at some point
  you know the keybindings by heart.
- **No permanently visible session name**: The session name only appears in
  *SESSION* mode, since I don't switch sessions without going there in any
  case.
- **No permanently visible swap layout**: Zellij does a pretty good job of
  figuring these out on its own, so I bound the keys for manually changing them
  to *PANE* mode.
- **Modes are only shown in Modes**: Normal mode doesn't have a visual
  identifier, so all you see during "regular" operation is the list of open
  tabs.


## Development

### Building

First, you must install the `wasm32-wasi` rust target like so:

```bash
rustup target add wasm32-wasi
```

Then you build the plugin like this:

```bash
cargo build
```

The project is configured to automatically compile to WASM so you don't have to
specify that manually.


### Testing

Run the (doc)tests with the following command:

```bash
cargo test --target "$(rustc -Vv | grep -oP "host: \K.*")"
```

> The `$(rustc ...)` bit figures out your hosts target triple automatically. If
> you don't trust the code, you can fill in your hosts target triple manually,
> too. In any case: The `--target` option must be defined! That is because the
> crate compiles against the `wasm32-wasi` target by default, but running tests
> for that target requires special tooling which didn't work the last time I
> tried it.

Beyond the doctests, you may also want to run the freshly built plugin to see
it in action. For this purpose, there are two layouts included in the repo
under the `assets/layouts/` folder. To use them, open a fresh terminal (without
a running zellij session) and run the following command:

```bash
zellij -l ./assets/layouts/plugin-bare.kdl
```

> The `plugin-dev-workspace.kdl` layout should, in theory, work using the
> `start-or-reload-plugin` action, allowing you to hot-reload the plugin in a
> running zellij session. For some reason it doesn't work currently. If you
> know what's wrong, I'd be happy to hear about it!


## But why?

Beyond what's shipped with zellij, there is also the [`zjstatus`][zjstatus]
plugin, which offers a lot of customizations and a widget system. I wanted
something simple, particularly something that goes well with my [own tabline
for neovim][tabline.nvim]. So I decided to have a stab at it and here we are.



[zjstatus]: https://github.com/dj95/zjstatus/tree/main
[tabline.nvim]: https://gitlab.com/hartang/nvim/tabline.nvim
[zellij_config]: https://gitlab.com/hartang/dotfiles/-/blob/main/.config/zellij/config.kdl
[zellij_docs_plugin_alias]: https://zellij.dev/documentation/plugin-aliases
[zellij_docs_layout]: https://zellij.dev/documentation/layouts
